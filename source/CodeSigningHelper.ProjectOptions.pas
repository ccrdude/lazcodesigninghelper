{
   @author(Patrick Michael Kolla-ten Venne [pk] <patrick@kolla-tenvenne.de>)
   @abstract(Project specific settings.)

   @preformatted(
// *****************************************************************************
// Copyright: © 2017-2021 Patrick Michael Kolla-ten Venne. All rights reserved.
// *****************************************************************************
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
// *****************************************************************************
// Changelog (new entries first):
// ---------------------------------------
// 2017-05-17  pk  ---  [CCD] Updated unit header.
// *****************************************************************************
   )
}

unit CodeSigningHelper.ProjectOptions;

{$IFDEF FPC}
{$mode objfpc}{$H+}
{$ENDIF FPC}

interface


uses
   Classes,
   SysUtils,
   LazIDEIntf,
   IDEOptionsIntf,
   IDEOptEditorIntf,
   IDEIntf,
   IniFiles,
   CodeSigningHelper.Options,
   CodeSigningHelper.ProjectSettings;

type

   { TCodeSigningProjectOptionsAppleCodeSign }

   TCodeSigningProjectOptionsAppleCodeSign = class(TPersistent)
   private
      FUseSpecificCertificate: boolean;
   protected
      procedure AssignTo(Dest: TPersistent); override;
      procedure LoadFromIni(AIni: TCustomIniFile; ASectionName: string);
      procedure SaveToIni(AIni: TCustomIniFile; ASectionName: string);
   public
      constructor Create;
      property UseSpecificCertificate: boolean read FUseSpecificCertificate write FUseSpecificCertificate;
   end;

   { TCodeSigningProjectOptions }

   TCodeSigningProjectOptions = class(TAbstractIDEOptions)
   private
      class var FInstance: TCodeSigningProjectOptions;
   private
      FAppleCodeSignCustom: TCodeSigningProjectOptionsAppleCodeSign;
      FAppleCodeSignOptions: TCodeSigningOptionsAppleCodeSign;
      //FGnuPGCustom: TCodeSigningProjectOptionsGnuPG;
      //FGnuPGOptions: TCodeSigningOptionsGnuPG;
      //FJavaKeyToolCustom: TCodeSigningProjectOptionsJavaKeyTool;
      //FJavaKeyToolOptions: TCodeSigningOptionsJavaKeyTool;
      //FMicrosoftSignToolCustom: TCodeSigningProjectOptionsMicrosoftSignTool;
      //FMicrosoftSignToolOptions: TCodeSigningOptionsMicrosoftSignTool;
      FProjectSettings: TCodeSigningCompilerSettings;
   protected
      procedure LoadFromIni(AIni: TCustomIniFile; ASectionName: string); //override;
      procedure SaveToIni(AIni: TCustomIniFile; ASectionName: string); //override;
      function GetConfigFilename: string; //override;
      function GetBuildModeID: string; //override;
   public
      class constructor Create;
      class destructor Destroy;
      class function GetGroupCaption: string; override;
      class function GetInstance: TAbstractIDEOptions; override;
   public
      constructor Create; //override;
      destructor Destroy; override;
      procedure Read;
      procedure Write;
      procedure Assign(Source: TPersistent); override;

      property ProjectSettings: TCodeSigningCompilerSettings read FProjectSettings;

      //property MicrosoftSignToolOptions: TCodeSigningOptionsMicrosoftSignTool read FMicrosoftSignToolOptions;
      property AppleCodeSignOptions: TCodeSigningOptionsAppleCodeSign read FAppleCodeSignOptions;
      //property JavaKeyToolOptions: TCodeSigningOptionsJavaKeyTool read FJavaKeyToolOptions;
      //property GnuPGOptions: TCodeSigningOptionsGnuPG read FGnuPGOptions;
      property AppleCodeSignCustom: TCodeSigningProjectOptionsAppleCodeSign read FAppleCodeSignCustom;
      //property GnuPGCustom: TCodeSigningProjectOptionsGnuPG read FGnuPGCustom;
      //property JavaKeyToolCustom: TCodeSigningProjectOptionsJavaKeyTool read FJavaKeyToolCustom;
      //property MicrosoftSignToolCustom: TCodeSigningProjectOptionsMicrosoftSignTool read FMicrosoftSignToolCustom;
   end;

implementation

uses
   Dialogs,
   IDEMsgIntf,
   IDEExternToolIntf,
   CodeSigningHelper.Debug,
   CodeSigningHelper.Strings;

{ TCodeSigningProjectOptionsAppleCodeSign }

procedure TCodeSigningProjectOptionsAppleCodeSign.AssignTo(Dest: TPersistent);
begin
   if Dest is TCodeSigningProjectOptionsAppleCodeSign then begin
      TCodeSigningProjectOptionsAppleCodeSign(Dest).FUseSpecificCertificate := Self.FUseSpecificCertificate;
   end;
end;

procedure TCodeSigningProjectOptionsAppleCodeSign.LoadFromIni(AIni: TCustomIniFile; ASectionName: string);
begin
   FUseSpecificCertificate := AIni.ReadBool(ASectionName, 'UseSpecificCertificate', False);
end;

procedure TCodeSigningProjectOptionsAppleCodeSign.SaveToIni(AIni: TCustomIniFile; ASectionName: string);
begin
   AIni.WriteBool(ASectionName, 'UseSpecificCertificate', FUseSpecificCertificate);
end;

constructor TCodeSigningProjectOptionsAppleCodeSign.Create;
begin
   UseSpecificCertificate := False;
end;


{ TCodeSigningProjectOptions }

procedure TCodeSigningProjectOptions.LoadFromIni(AIni: TCustomIniFile; ASectionName: string);
begin
   FAppleCodeSignCustom.LoadFromIni(AIni, ASectionName + '.AppleCodeSign');
   FAppleCodeSignOptions.LoadFromIni(AIni, ASectionName + '.AppleCodeSign');
end;

procedure TCodeSigningProjectOptions.SaveToIni(AIni: TCustomIniFile; ASectionName: string);
begin
   CodeSigningLogInformation(Self, Format('TCodeSigningProjectOptions.SaveToIni:'#13#10'Saving project options to section "%s" to file %s', [ASectionName, AIni.FileName]));
   FAppleCodeSignCustom.SaveToIni(AIni, ASectionName + '.AppleCodeSign');
   FAppleCodeSignOptions.SaveToIni(AIni, ASectionName + '.AppleCodeSign');
end;

function TCodeSigningProjectOptions.GetConfigFilename: string;
begin
   Result := '';
   try
      if Assigned(LazarusIDE) then begin
         if Assigned(LazarusIDE.ActiveProject) then begin
            if Assigned(LazarusIDE.ActiveProject.MainFile) then begin
               Result := LazarusIDE.ActiveProject.MainFile.Filename + '.codesigning.ini';
            end;
         end;
      end;
   except
      on E: Exception do begin
         AddIDEMessage(mluError, rsCodeSigningErrorLazarusIDEActiveProjectMainFileFilename + LineEnding + E.Message, '', 0, 0, 'CodeSigning Debug');
         Result := '';
      end;
   end;
end;

function TCodeSigningProjectOptions.GetBuildModeID: string;
begin
   Result := 'default';
   try
      if Assigned(LazarusIDE) then begin
         if Assigned(LazarusIDE.ActiveProject) then begin
            Result := LazarusIDE.ActiveProject.ActiveBuildModeID;
         end;
      end;
   except
      on E: Exception do begin
         AddIDEMessage(mluError, rsCodeSigningErrorLazarusIDEActiveProjectMainFileFilename + LineEnding + E.Message, '', 0, 0, 'CodeSigning Debug');
         Result := '';
      end;
   end;
end;

constructor TCodeSigningProjectOptions.Create;
begin
   FAppleCodeSignOptions := TCodeSigningOptionsAppleCodeSign.Create;
   FAppleCodeSignCustom := TCodeSigningProjectOptionsAppleCodeSign.Create;
   FProjectSettings := TCodeSigningCompilerSettings.Create;
   inherited Create;
end;

destructor TCodeSigningProjectOptions.Destroy;
begin
   FProjectSettings.Free;
   FAppleCodeSignOptions.Free;
   FAppleCodeSignCustom.Free;
   inherited Destroy;
end;

procedure TCodeSigningProjectOptions.Read;
var
   sFilename: string;
begin
   CodeSigningLogInformation(Self, 'Read');
   sFilename := GetConfigFilename;
   if Length(sFilename) = 0 then begin
      CodeSigningLogInformation(Self, 'Unable to read options, no config filename available.');
      Exit;
   end;
   try
      ProjectSettings.LoadFromIniFile(sFilename);
   except
      on E: Exception do begin
         if Assigned(IDEMessagesWindow) then begin
            IDEMessagesWindow.AddCustomMessage(mluError, 'TCodeSigningOptions.Read: ' + E.Message, '', 0, 0, 'CodeSigning Debug');
         end else begin
            ShowMessage(E.Message + LineEnding + DumpExceptionCallStack(E));
         end;
      end;
   end;
end;

procedure TCodeSigningProjectOptions.Write;
begin
   CodeSigningLogInformation(Self, 'Write');
   // no longer needed, frame saves itself
end;

class constructor TCodeSigningProjectOptions.Create;
begin
   FInstance := nil;
end;

class destructor TCodeSigningProjectOptions.Destroy;
begin
   FInstance.Free;
end;

class function TCodeSigningProjectOptions.GetGroupCaption: string;
begin
   Result := rsCodeSigningProjectOptionsGroupName;
end;

class function TCodeSigningProjectOptions.GetInstance: TAbstractIDEOptions;
begin
   if not Assigned(FInstance) then begin
      FInstance := TCodeSigningProjectOptions.Create;
   end;
   Result := FInstance;
end;

procedure TCodeSigningProjectOptions.Assign(Source: TPersistent);
begin
   if Source is TCodeSigningProjectOptions then begin
      AppleCodeSignCustom.Assign(TCodeSigningProjectOptions(Source).AppleCodeSignCustom);
      inherited Assign(Source);
   end else begin
      inherited Assign(Source);
   end;
end;

initialization

   RegisterIDEOptionsGroup(GroupProject, TCodeSigningProjectOptions);

end.
