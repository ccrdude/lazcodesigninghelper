{
   @author(Patrick Michael Kolla-ten Venne [pk] <patrick@kolla-tenvenne.de>)
   @abstract(Codesigning implementation for Microsofts signtool.exe.)

   @preformatted(
// *****************************************************************************
// Copyright: © 2017-2022 Patrick Michael Kolla-ten Venne. All rights reserved.
// *****************************************************************************
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
// *****************************************************************************
// Changelog (new entries first):
// ---------------------------------------
// 2022-05-31  pk  30m  Adding first Azure Code Signing related entries
// 2017-05-17  pk  10m  Fixed verify issue (missing parameter for the policy).
// 2017-05-17  pk  30m  Moved from old PepiMK.Authenticode.Sign.External unit.
// 2017-05-17  pk  ---  [CCD] Updated unit header.
// *****************************************************************************
   )
}

unit PepiMK.Signing.MicrosoftSignTool;

{$IFDEF FPC}
{$mode objfpc}{$H+}
{$ENDIF FPC}

interface

uses
   Classes,
   SysUtils,
   IniFiles,
   PepiMK.Signing.Base;

type
   TCodeSignAlgorithm = (csaSHA1, csaSHA256);
   TCodeSignAlgorithms = set of TCodeSignAlgorithm;
   TCodeSignPageHashing = (csphYes, csphNo, csphDefault);
   TMicrosoftSignToolCodeSigningMode = (msstcsmLocal, msstcsmAzure);

   { TMicrosoftSignToolCrossSigning }

   TMicrosoftSignToolCrossSigning = class(TPersistent)
   private
      FActive: boolean;
      FFilename: WideString;
   protected
      procedure AssignTo(Dest: TPersistent); override;
   public
      constructor Create;
      procedure LoadFromIni(AIni: TCustomIniFile; ASection: string);
      procedure SaveToIni(AIni: TCustomIniFile; ASection: string);
      property Active: boolean read FActive write FActive;
      property Filename: WideString read FFilename write FFilename;
   end;

   TMicrosoftSignToolTimestampingMethod = (tstClassic, tstRFC3161, tstSeal);

   { TMicrosoftSignToolTimestamping }

   TMicrosoftSignToolTimestamping = class(TPersistent)
   private
      FActive: boolean;
      FMethod: TMicrosoftSignToolTimestampingMethod;
      FServer: string;
      FServerRFC3161: string;
   protected
      procedure AssignTo(Dest: TPersistent); override;
   public
      constructor Create;
      procedure LoadFromIni(AIni: TCustomIniFile; ASection: string);
      procedure SaveToIni(AIni: TCustomIniFile; ASection: string);
      property Active: boolean read FActive write FActive;
      property Server: string read FServer write FServer;
      property ServerRFC3161: string read FServerRFC3161 write FServerRFC3161;
      property Method: TMicrosoftSignToolTimestampingMethod read FMethod write FMethod;
   end;

   { TMicrosoftSignToolFlags }

   TMicrosoftSignToolFlags = class(TPersistent)
   private
      FUsePageHashing: TCodeSignPageHashing;
      FUseWindowsSystemComponentVerification: boolean;
   protected
      procedure AssignTo(Dest: TPersistent); override;
   public
      constructor Create;
      procedure LoadFromIni(AIni: TCustomIniFile; ASection: string);
      procedure SaveToIni(AIni: TCustomIniFile; ASection: string);
   public
      property UsePageHashing: TCodeSignPageHashing read FUsePageHashing write FUsePageHashing;
      property UseWindowsSystemComponentVerification: boolean read FUseWindowsSystemComponentVerification write FUseWindowsSystemComponentVerification;
   end;

   { TMicrosoftSignToolAzureCodeSigning }

   TMicrosoftSignToolAzureCodeSigning = class(TPersistent)
   private
      FAccountName: string;
      FDLibPath: string;
      FEndPoint: string;
      FProfileName: string;
   protected
      procedure AssignTo(Dest: TPersistent); override;
   public
      constructor Create;
      procedure LoadFromIni(AIni: TCustomIniFile; ASection: string);
      procedure SaveToIni(AIni: TCustomIniFile; ASection: string);
   public
      property EndPoint: string read FEndPoint write FEndPoint;
      property AccountName: string read FAccountName write FAccountName;
      property ProfileName: string read FProfileName write FProfileName;
      property DLibPath: string read FDLibPath write FDLibPath;
   end;

   { TMicrosoftSignToolSigner }

   TMicrosoftSignToolSigner = class(TCustomFileSigner)
   private
      FACS: TMicrosoftSignToolAzureCodeSigning;
      FAlgorithm: TCodeSignAlgorithm;
      FAlgorithms: TCodeSignAlgorithms;
      FCrossSigning: TMicrosoftSignToolCrossSigning;
      FDescriptionSubject: WideString;
      FDescriptionURL: ansistring;
      FFlags: TMicrosoftSignToolFlags;
      FMode: TMicrosoftSignToolCodeSigningMode;
      FTimestamping: TMicrosoftSignToolTimestamping;
   protected
      procedure ConstructSignParameters(AAdder: TConstructParametersProc); override;
      procedure ConstructVerifyParameters(AAdder: TConstructParametersProc); override;
      function ReadDefaultsIniSectionName: ansistring; override;
      procedure CreateAzureCodeSigningMetaData(AFilename: string);
   public
      class function SupportsLazarusTargetOS(AOS: string): boolean; override;
   public
      constructor Create; override;
      destructor Destroy; override;
      function SignFile(const AFilename: WideString): boolean; override;
      property DescriptionURL: ansistring read FDescriptionURL write FDescriptionURL;
      property DescriptionSubject: WideString read FDescriptionSubject write FDescriptionSubject;
      property Algorithms: TCodeSignAlgorithms read FAlgorithms write FAlgorithms;
      property Flags: TMicrosoftSignToolFlags read FFlags;
      property ACS: TMicrosoftSignToolAzureCodeSigning read FACS;
      property Mode: TMicrosoftSignToolCodeSigningMode read FMode write FMode;
      property CrossSigning: TMicrosoftSignToolCrossSigning read FCrossSigning;
      property Timestamping: TMicrosoftSignToolTimestamping read FTimestamping;
   end;

function GetSignToolExecutable(var ASigningExecutable: WideString): boolean;

implementation

uses
   fpjson;

function GetSignToolExecutable(var ASigningExecutable: WideString): boolean;
begin
   Result := False;
   if FileExists(ExtractFilePath(ParamStr(0)) + 'signtool.exe') then begin
      ASigningExecutable := ExtractFilePath(UTF8Decode(ParamStr(0))) + 'signtool.exe';
      Result := True;
      Exit;
   end;
   if FileExists('c:\Program Files (x86)\Windows Kits\10\bin\x86\signtool.exe') then begin
      ASigningExecutable := 'c:\Program Files (x86)\Windows Kits\10\bin\x86\signtool.exe';
      Result := True;
      Exit;
   end;
   if FileExists('c:\Program Files (x86)\Windows Kits\10\bin\10.0.17134.0\x86\signtool.exe') then begin
      ASigningExecutable := 'c:\Program Files (x86)\Windows Kits\10\bin\10.0.17134.0\x86\signtool.exe';
      Result := True;
      Exit;
   end;
end;

{ TMicrosoftSignToolAzureCodeSigning }

procedure TMicrosoftSignToolAzureCodeSigning.AssignTo(Dest: TPersistent);
begin
   if Dest is TMicrosoftSignToolAzureCodeSigning then begin
      TMicrosoftSignToolAzureCodeSigning(Dest).Endpoint := Self.Endpoint;
      TMicrosoftSignToolAzureCodeSigning(Dest).AccountName := Self.AccountName;
      TMicrosoftSignToolAzureCodeSigning(Dest).ProfileName := Self.ProfileName;
      TMicrosoftSignToolAzureCodeSigning(Dest).DLibPath := Self.DLibPath;
   end;
end;

constructor TMicrosoftSignToolAzureCodeSigning.Create;
begin

end;

procedure TMicrosoftSignToolAzureCodeSigning.LoadFromIni(AIni: TCustomIniFile; ASection: string);
begin
   FEndpoint := AIni.ReadString(ASection, 'ACS.Endpoint', '');
   FAccountName := AIni.ReadString(ASection, 'ACS.AccountName', '');
   FProfileName := AIni.ReadString(ASection, 'ACS.ProfileName', '');
   FDLibPath := AIni.ReadString(ASection, 'ACS.DLibPath', '');
end;

procedure TMicrosoftSignToolAzureCodeSigning.SaveToIni(AIni: TCustomIniFile; ASection: string);
begin
   AIni.WriteString(ASection, 'ACS.Endpoint', FEndpoint);
   AIni.WriteString(ASection, 'ACS.AccountName', FAccountName);
   AIni.WriteString(ASection, 'ACS.ProfileName', FProfileName);
   AIni.WriteString(ASection, 'ACS.DLibPath', FDLibPath);
end;

{ TMicrosoftSignToolFlags }

procedure TMicrosoftSignToolFlags.AssignTo(Dest: TPersistent);
begin
   if Dest is TMicrosoftSignToolFlags then begin
      TMicrosoftSignToolFlags(Dest).UsePageHashing := Self.UsePageHashing;
      TMicrosoftSignToolFlags(Dest).UseWindowsSystemComponentVerification := Self.UseWindowsSystemComponentVerification;
   end;
end;

constructor TMicrosoftSignToolFlags.Create;
begin
   FUseWindowsSystemComponentVerification := False;
   FUsePageHashing := csphDefault;
end;

procedure TMicrosoftSignToolFlags.LoadFromIni(AIni: TCustomIniFile; ASection: string);
begin
   FUsePageHashing := TCodeSignPageHashing(AIni.ReadInteger(ASection, 'Flags.PageHashing', integer(FUsePageHashing)));
   FUseWindowsSystemComponentVerification := AIni.ReadBool(ASection, 'Flags.WindowsSystemComponentVerification', FUseWindowsSystemComponentVerification);
end;

procedure TMicrosoftSignToolFlags.SaveToIni(AIni: TCustomIniFile; ASection: string);
begin
   AIni.WriteInteger(ASection, 'Flags.PageHashing', integer(FUsePageHashing));
   AIni.WriteBool(ASection, 'Flags.WindowsSystemComponentVerification', FUseWindowsSystemComponentVerification);
end;

{ TMicrosoftSignToolTimestamping }

procedure TMicrosoftSignToolTimestamping.AssignTo(Dest: TPersistent);
begin
   if Dest is TMicrosoftSignToolTimestamping then begin
      TMicrosoftSignToolTimestamping(Dest).Active := Self.Active;
      TMicrosoftSignToolTimestamping(Dest).Server := Self.Server;
      TMicrosoftSignToolTimestamping(Dest).ServerRFC3161 := Self.ServerRFC3161;
   end;
end;

constructor TMicrosoftSignToolTimestamping.Create;
begin
   FActive := False;
   FMethod := tstRFC3161;
end;

procedure TMicrosoftSignToolTimestamping.LoadFromIni(AIni: TCustomIniFile; ASection: string);
begin
   FActive := AIni.ReadBool(ASection, 'Timestamping.Active', FActive);
   FServer := AIni.ReadString(ASection, 'Timestamping.Server', FServer);
   FServerRFC3161 := AIni.ReadString(ASection, 'Timestamping.ServerRFC3161', FServerRFC3161);
end;

procedure TMicrosoftSignToolTimestamping.SaveToIni(AIni: TCustomIniFile; ASection: string);
begin
   AIni.WriteBool(ASection, 'Timestamping.Active', FActive);
   AIni.WriteString(ASection, 'Timestamping.Server', FServer);
   AIni.WriteString(ASection, 'Timestamping.ServerRFC3161', FServerRFC3161);
end;

{ TMicrosoftSignToolCrossSigning }

procedure TMicrosoftSignToolCrossSigning.AssignTo(Dest: TPersistent);
begin
   if Dest is TMicrosoftSignToolCrossSigning then begin
      TMicrosoftSignToolCrossSigning(Dest).Active := Self.Active;
      TMicrosoftSignToolCrossSigning(Dest).Filename := Self.Filename;
   end;
end;

constructor TMicrosoftSignToolCrossSigning.Create;
begin
   FActive := False;
end;

procedure TMicrosoftSignToolCrossSigning.LoadFromIni(AIni: TCustomIniFile; ASection: string);
begin
   FActive := AIni.ReadBool(ASection, 'CrossSigning.Active', FActive);
   FFilename := UTF8Decode(AIni.ReadString(ASection, 'CrossSigning.Filename', UTF8Encode(FFilename)));
end;

procedure TMicrosoftSignToolCrossSigning.SaveToIni(AIni: TCustomIniFile; ASection: string);
begin
   AIni.WriteBool(ASection, 'CrossSigning.Active', FActive);
   AIni.WriteString(ASection, 'CrossSigning.Filename', UTF8Encode(FFilename));
end;

{ TMicrosoftSignToolSigner }

procedure TMicrosoftSignToolSigner.ConstructSignParameters(AAdder: TConstructParametersProc);
var
   sJSONFilename: string;
begin
   AAdder('sign');
   AAdder('/v');
   AAdder('/debug');
   case Flags.UsePageHashing of
      csphYes: AAdder('/ph');
      csphNo: AAdder('/nph');
   end;
   case Mode of
      msstcsmLocal: begin
         case Certificate.Source of
            cscsStoreByHash:
            begin
               AAdder('/sha1');
               AAdder(UTF8Decode(Certificate.Hash));
            end;
            cscsFileAsPFX:
            begin
               AAdder('/f');
               AAdder(Certificate.Filename);
               if Length(Certificate.FilePassword) > 0 then begin
                  AAdder('/p');
                  AAdder(Certificate.FilePassword);
               end;
            end;
            cscsStoreBySubstring:
            begin
               AAdder('/n');
               AAdder(UTF8Decode(Certificate.Substring));
            end;
         end;
      end;
      msstcsmAzure: begin
         (*
         + sign
         + /v
         + /debug
         + /fd SHA256
         + /tr "http://timestamp.acs.microsoft.com"
         + /td SHA256
         /dlib "c:\Software\Azure.CodeSigning.Dlib.1.0.18\bin\x64\Azure.CodeSigning.Dlib.dll"
         /dmdf "d:\Development\Projects\spybot3\bin\AntiBeacon-Debug\signing-metadata.json"
         + "d:\Development\Projects\spybot3\bin\AntiBeacon-Debug\Spybot3AntiBeacon.exe"
         *)
         AAdder('/dlib');
         AAdder(ACS.DLibPath);
         AAdder('/dmdf');
         sJSONFilename := Self.Filename + '.acs-metadata.json';
         CreateAzureCodeSigningMetaData(sJSONFilename);
         AAdder(sJSONFilename);
      end;
   end;
   if Length(FDescriptionSubject) > 0 then begin
      AAdder('/d');
      AAdder(FDescriptionSubject);
   end;
   if Length(FDescriptionURL) > 0 then begin
      AAdder('/du');
      AAdder(UTF8Decode(FDescriptionURL));
   end;
   if FileExists(CrossSigning.Filename) and (CrossSigning.Active) then begin
      AAdder('/ac');
      AAdder(CrossSigning.Filename);
   end;
   if AppendSignature then begin
      AAdder('/as');
   end;
   case FAlgorithm of
      csaSHA1:
      begin
         AAdder('/fd');
         AAdder('sha1');
      end;
      csaSHA256:
      begin
         AAdder('/fd');
         AAdder('sha256');
      end;
   end;
   if Flags.UseWindowsSystemComponentVerification then begin
      AAdder('/uw');
   end;
   if Timestamping.Active then begin
      case Timestamping.Method of
         tstClassic:
         begin
            AAdder('/t');
            AAdder(UTF8Decode(Timestamping.Server));
         end;
         tstRFC3161:
         begin
            AAdder('/tr');
            AAdder(UTF8Decode(Timestamping.ServerRFC3161));
         end;
         tstSeal:
         begin
            AAdder('/tseal');
            AAdder(UTF8Decode(Timestamping.ServerRFC3161));
         end;
      end;
      AAdder('/td');
      AAdder('sha256');
   end;
   AAdder(Filename);
end;

procedure TMicrosoftSignToolSigner.ConstructVerifyParameters(AAdder: TConstructParametersProc);
begin
   AAdder('verify');
   AAdder('/v');
   AAdder('/debug');
   AAdder('/all'); // Verify all signatures in a file with multiple signatures.
   AAdder('/pa'); // Use the "Default Authenticode" Verification Policy.
                  //AAdder('/sl'); // Verify sealing signatures for supported file types.
   AAdder('/tw'); // Generate a Warning if the signature is not timestamped.
   AAdder(Filename);
end;

function TMicrosoftSignToolSigner.ReadDefaultsIniSectionName: ansistring;
begin
   Result := 'Microsoft.SignTool';
end;

procedure TMicrosoftSignToolSigner.CreateAzureCodeSigningMetaData(AFilename: string);
var
   o: TJSONObject;
   s: rawbytestring;
   ms: TMemoryStream;
begin
   // TODO : create temporary json file with metadata, and pass here
   (*
   {
   "Endpoint": "https://weu.codesigning.azure.net/",
   "CodeSigningAccountName": "CodeSigningLazarus",
   "CertificateProfileName": "ProfileLaz",
   "CorrelationId": "LazCodeSigningHelper"
   }
   *)
   o := TJSONObject.Create;
   o.Add('Endpoint', Self.ACS.EndPoint);
   o.Add('CodeSigningAccountName', Self.ACS.AccountName);
   o.Add('CertificateProfileName', Self.ACS.ProfileName);
   s := o.AsJSON;
   ms := TMemoryStream.Create;
   try
      if Length(s) > 0 then begin
         ms.Write(s[1], Length(s));
      end;
      ms.SaveToFile(AFilename);
   finally
      ms.Free;
   end;
end;

class function TMicrosoftSignToolSigner.SupportsLazarusTargetOS(AOS: string): boolean;
begin
   Result := SameTexT(AOS, 'Win32') or SameText(AOS, 'Win64');
end;

constructor TMicrosoftSignToolSigner.Create;
var
   s: WideString;

begin
   inherited Create;
   FMode := msstcsmLocal;
   FFlags := TMicrosoftSignToolFlags.Create;
   FACS := TMicrosoftSignToolAzureCodeSigning.Create;
   FCrossSigning := TMicrosoftSignToolCrossSigning.Create;
   FTimestamping := TMicrosoftSignToolTimestamping.Create;
   s := SigningExecutable;
   if GetSignToolExecutable(s) then begin
      SigningExecutable := s;
   end;
end;

destructor TMicrosoftSignToolSigner.Destroy;
begin
   FFlags.Free;
   FACS.Free;
   FCrossSigning.Free;
   FTimestamping.Free;
   inherited Destroy;
end;

function TMicrosoftSignToolSigner.SignFile(const AFilename: WideString): boolean;
var
   a: TCodeSignAlgorithm;
   slOutput: TStringList;
   slErrors: TStringList;
   bFirstApplied: boolean;
   sJSONFilename: string;
begin
   Result := True;
   bFirstApplied := False;
   slOutput := TStringList.Create;
   slErrors := TStringList.Create;
   try
      for a in TCodeSignAlgorithm do begin
         if a in Algorithms then begin
            FAlgorithm := a;
            AppendSignature := (bFirstApplied);
            Result := Result and inherited SignFile(AFilename);
            slOutput.AddStrings(Outcome.Output);
            slErrors.AddStrings(Outcome.Errors);
            bFirstApplied := True;
         end;
      end;
      //sJSONFilename := Self.Filename + '.acs-metadata.json';
      //if FileExists(sJSONFilename) then begin
      //   DeleteFile(sJSONFilename);
      //end;
   finally
      Outcome.Output.Assign(slOutput);
      slOutput.Free;
      Outcome.Errors.Assign(slErrors);
      slErrors.Free;
   end;
end;

end.
