unit CodeSigningHelper.Options.Profiles.Frame;

{$mode objfpc}{$H+}

interface

uses
   Classes,
   SysUtils,
   FileUtil,
   Forms,
   Controls,
   StdCtrls,
   EditBtn,
   PepiMK.Signing.Base,
   PepiMK.Signing.MicrosoftSignTool,
   CodeSigningHelper.Options,
   CodeSigningHelper.ProjectOptions,
   CodeSigningHelper.Frame.Profiles,
   IDEOptEditorIntf,
   IDEOptionsIntf;

type

   { TFrameOptionsCodeSigningProfiles }

   TFrameOptionsCodeSigningProfiles = class(TAbstractIDEOptionsEditor)
      FrameCodeSigningProfiles1: TFrameCodeSigningProfiles;
   private
      { private declarations }
   public
      { public declarations }
      function GetTitle: string; override;
      procedure Setup({%H-}ADialog: TAbstractOptionsEditorDialog); override;
      procedure ReadSettings(AOptions: TAbstractIDEOptions); override;
      procedure WriteSettings(AOptions: TAbstractIDEOptions); override;
      class function SupportedOptionsClass: TAbstractIDEOptionsClass; override;
   end;

implementation

uses
   Dialogs,
   IDEIntf,
   IDEExternToolIntf,
   CodeSigningHelper.Strings,
   CodeSigningHelper.Debug;

{$R *.lfm}

resourcestring
   rsCodeSigningProfilesTitle = 'CodeSigning Profiles';

{ TFrameOptionsCodeSigningProfiles }

function TFrameOptionsCodeSigningProfiles.GetTitle: string;
begin
   Result := rsCodeSigningProfilesTitle;
end;

procedure TFrameOptionsCodeSigningProfiles.Setup(ADialog: TAbstractOptionsEditorDialog);
begin
   CodeSigningLogInformation(Self, 'Setup');
   ReadSettings(TCodeSigningOptions.GetInstance);
end;

procedure TFrameOptionsCodeSigningProfiles.ReadSettings(AOptions: TAbstractIDEOptions);
var
   o: TCodeSigningOptions;
begin
   CodeSigningLogInformation(Self, 'ReadSettings');
   o := TCodeSigningOptions(AOptions);
   o.Read;
   FrameCodeSigningProfiles1.AssignProfiles(o.Profiles);
end;

procedure TFrameOptionsCodeSigningProfiles.WriteSettings(AOptions: TAbstractIDEOptions);
var
   o: TCodeSigningOptions;
begin
   CodeSigningLogInformation(Self, 'WriteSettings');
   o := TCodeSigningOptions(AOptions);
   o.Write;
end;

class function TFrameOptionsCodeSigningProfiles.SupportedOptionsClass: TAbstractIDEOptionsClass;
begin
   Result := TCodeSigningOptions;
end;

initialization
   RegisterIDEOptionsEditor(CodeSigningOptionGroup, TFrameOptionsCodeSigningProfiles, 5, NoParent, True);

end.
