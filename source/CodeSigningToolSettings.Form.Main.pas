unit CodeSigningToolSettings.Form.Main;

{$mode objfpc}{$H+}

interface

uses
   Classes,
   SysUtils,
   Forms,
   Controls,
   Graphics,
   Dialogs,
   ComCtrls,
   CodeSigningHelper.Profiles,
   CodeSigningHelper.ProjectSettings,
   CodeSigningHelper.ProjectOptions,
   CodeSigningHelper.Frame.Profiles,
   CodeSigningHelper.Frame.ProjectSettings;

type

   { TFormCodeSigningFormTests }

   TFormCodeSigningFormTests = class(TForm)
      FrameCodeSigningProfiles1: TFrameCodeSigningProfiles;
      FrameCodeSigningProjectSettings1: TFrameCodeSigningProjectSettings;
      pcTests: TPageControl;
      tabProfiles: TTabSheet;
      tabProjectSettings: TTabSheet;
      procedure FormCreate(Sender: TObject);
      procedure FormDestroy(Sender: TObject);
      procedure FormShow(Sender: TObject);
   private
      FProfiles: TCodeSigningProfiles;
      FSettings: TCodeSigningCompilerSettings;
   public

   end;

var
   FormCodeSigningFormTests: TFormCodeSigningFormTests;

implementation

{$R *.lfm}

{ TFormCodeSigningFormTests }

procedure TFormCodeSigningFormTests.FormCreate(Sender: TObject);
begin
   FProfiles := TCodeSigningProfiles.Create;
   FSettings := TCodeSigningCompilerSettings.Create;
end;

procedure TFormCodeSigningFormTests.FormDestroy(Sender: TObject);
begin
   FProfiles.Free;
   FSettings.Free;
end;

procedure TFormCodeSigningFormTests.FormShow(Sender: TObject);
begin
   FProfiles.LoadFromIniFile('C:\Development\codesigning.ini');
   FSettings.LoadFromIniFile('C:\Development\codesigning-projectsettings.ini');
   FrameCodeSigningProfiles1.AssignProfiles(FProfiles);
   FrameCodeSigningProjectSettings1.AssignProfiles(FProfiles);
   FrameCodeSigningProjectSettings1.AssignSettings(FSettings);
end;

end.
