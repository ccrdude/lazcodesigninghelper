{
   @author(Patrick Michael Kolla-ten Venne [pk] <patrick@kolla-tenvenne.de>)
   @abstract(Helper making codesigning available in the Lazarus IDE.)

   @preformatted(
// *****************************************************************************
// Copyright: © 2017-2022 Patrick Michael Kolla-ten Venne. All rights reserved.
// *****************************************************************************
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
// *****************************************************************************
// Changelog (new entries first):
// ---------------------------------------
// 2017-05-17  pk  20m  Updated to new PepiMK.Signing.* units.
// 2017-05-17  pk  10m  Restructured into submenus.
// 2017-05-11  pk  ---  [CCD] Updated unit header.
// *****************************************************************************
   )
}

unit CodeSigningHelper.Menu;

{$IFDEF FPC}
{$mode objfpc}{$H+}
{$ENDIF FPC}

interface

uses
   Classes,
   SysUtils,
   MenuIntf,
   MacroIntf,
   ProjectIntf,
   LazIDEIntf,
   IDEMsgIntf,
   IDEOptEditorIntf, // for RegisterIDEOptionsGroup
   IDEExternToolIntf,
   LazarusPackageIntf,
   IDEOptionsIntf,
   Forms,
   Controls,
   Dialogs,
   CodeSigningHelper.Options,
   CodeSigningHelper.ProjectOptions,
   CodeSigningHelper.Profiles,
   CodeSigningHelper.ProjectSettings,
   CodeSigningHelper.Strings,
   PepiMK.Signing.Base,
   PepiMK.Signing.MicrosoftSignTool,
   PepiMK.Signing.AppleCodeSign,
   PepiMK.Signing.JavaKeyTool,
   PepiMK.Signing.GnuPG;

const
   SCodeSigningInterface = '{68BA16C3-CABE-4169-B0DD-388F513FFC48}';

type
   ICodeSigningInterface = interface
      [SCodeSigningInterface]
      function CertificateSignExecutable(AFilename: string; ACustomDescription: string = ''; AViewName: string = ''): boolean;
      function CertificateVerifyExecutable(AFilename: string; AViewName: string = ''): boolean;
   end;

   { TCodeSigningHelper }

   TCodeSigningHelper = class(TInterfacedPersistent, ICodeSigningInterface)
   private
   class var FInstance: TCodeSigningHelper;
   private
      FOtherTargetDialog: TOpenDialog;
      FMenuSectionCodeSigning: TIDEMenuSection;
      FMenuItemCodeSign: TIDEMenuCommand;
      FMenuItemCodeSignOther: TIDEMenuCommand;
      FMenuItemCodeVerify: TIDEMenuCommand;
      FMenuItemCodeVerifyOther: TIDEMenuCommand;
      FMenuSectionGnuPGSigning: TIDEMenuSection;
      function GetOutputExecutableFilename(out AFilename: string): boolean;
      procedure ProcessOutputToMessage(AResult: boolean; AOutcome: TCustomFileSignerResult; AView: string);
      procedure DoProjectBuildingFinished({%H-}ASender: TObject; ABuildSuccessful: boolean);
      procedure DoLazarusBuildingFinished({%H-}ASender: TObject; ABuildSuccessful: boolean);
      function DoProjectOpened({%H-}ASender: TObject; AProject: TLazProject): TModalResult;
      function DoProjectClose({%H-}ASender: TObject; AProject: TLazProject): TModalResult;
      function ExtractDescriptionURL(out AURL: string): boolean;
      procedure AddHandlers();
      procedure RemoveHandlers();
   private
      procedure DoIDEClose({%H-}Sender: TObject);
      procedure DoCertificateSign({%H-}Sender: TObject);
      procedure DoCertificateSignOther({%H-}Sender: TObject);
      procedure DoCertificateVerify({%H-}Sender: TObject);
      procedure DoCertificateVerifyOther({%H-}Sender: TObject);
   protected
      procedure AssignOptionsToAppleCodeSign(var AFilename: string; ASigner: TAppleCodeSignSigner; AAllowProjectSpecificOptions: boolean = True;
      {%H-}ACustomDescription: string = ''; AViewName: string = '');
      procedure AssignOptionsToGnuPG(var {%H-}AFilename: string; ASigner: TGnuPGSigner; ASetting: TGnuPGCompilerSettings);
      procedure AssignOptionsToMicrosoftSignTool(var {%H-}AFilename: string; ASigner: TMicrosoftSignToolSigner; ASetting: TMicrosoftSignToolCodeSigningCompilerSettings);
      procedure PrintMicrosoftSignToolOptions(ASigner: TMicrosoftSignToolSigner; AViewName: string);
      procedure PrintCertificate(ACertificate: TCustomFileSignerCertificate; AView: string);
      procedure TestMicrosoftSignToolProfile(AProfile: TMicrosoftSignToolCodeSigningProfile);
   public
      class constructor Create;
      class destructor Destroy;
   public
      constructor Create;
      destructor Destroy; override;
      procedure CreateMainMenuSubMenu();
      function GetTargetOS: string;
   public
      { public methods for ICodeSigningInterface }
      function SignExecutableFileWithSpecificSetting(AFilename: string; ASetting: TCustomCodeSigningCompilerSettings; ACustomDescription: string = ''): boolean;
      function SignExecutableFileWithAllSettings(AFilename: string; ACustomDescription: string = ''): boolean;
      function VerifyExecutableFileWithAllSettings(AFilename: string): boolean;
      function CertificateSignExecutable(AFilename: string; {%H-}ACustomDescription: string = ''; {%H-}AViewName: string = ''): boolean;
      function CertificateVerifyExecutable(AFilename: string; {%H-}AViewName: string = ''): boolean;
   published
      class function Instance: TCodeSigningHelper;
   end;

procedure Register;

implementation

uses
   RegExpr,
   {$IFDEF Darwin}
   BaseUnix,
   Unix,
   {$ENDIF Darwin}
   CompOptsIntf,
   FileUtil,
   FileCtrl,
   CodeSigningHelper.Debug;

procedure Register;
begin
   TCodeSigningHelper.Instance.CreateMainMenuSubMenu();
end;

{ TCodeSigningHelper }

function TCodeSigningHelper.GetOutputExecutableFilename(out AFilename: string): boolean;
var
   co: TLazCompilerOptions;
begin
   AFilename := '$(OutputFile)';
   Result := IDEMacros.SubstituteMacros(AFilename);
   if (Pos('$(', Result)>0) then begin
      AFilename := '$(TargetFile)';
      Result := IDEMacros.SubstituteMacros(AFilename);
   end;
   Exit;
   AddIDEMessage(mluNote, 'GetOutputExecutableFilename#2: ' + AFilename, '', 0, 0, 'xxx');
   // now the proper new approach
   if not Assigned(LazarusIDE) then begin
      Exit;
   end;
   if not Assigned(LazarusIDE.ActiveProject) then begin
      Exit;
   end;
   co := LazarusIDE.ActiveProject.LazBuildModes.BuildModes[LazarusIDE.ActiveProject.LazBuildModes.IndexOf(LazarusIDE.ActiveProject.ActiveBuildModeID)].LazCompilerOptions;
   AFilename := co.TargetFilename + co.TargetFileExt;
   AddIDEMessage(mluNote, 'GetOutputExecutableFilename#3: ' + AFilename, '', 0, 0, 'xxx');
   IDEMacros.SubstituteMacros(AFilename);
   AddIDEMessage(mluNote, 'GetOutputExecutableFilename#4: ' + AFilename, '', 0, 0, 'xxx');
   Result := True;
end;

procedure TCodeSigningHelper.ProcessOutputToMessage(AResult: boolean; AOutcome: TCustomFileSignerResult; AView: string);
var
   i: integer;
begin
   if AResult then begin
      for i := 0 to Pred(AOutcome.Output.Count) do begin
         if Length(AOutcome.Output[i]) > 0 then begin
            AddIDEMessage(mluNote, AOutcome.Output[i], '', 0, 0, AView);
         end;
      end;
      for i := 0 to Pred(AOutcome.Errors.Count) do begin
         if Length(AOutcome.Errors[i]) > 0 then begin
            AddIDEMessage(mluError, AOutcome.Errors[i], '', 0, 0, AView);
         end;
      end;
   end else begin
      for i := 0 to Pred(AOutcome.Output.Count) do begin
         if Length(AOutcome.Output[i]) > 0 then begin
            AddIDEMessage(mluNote, AOutcome.Output[i], '', 0, 0, AView);
         end;
      end;
      for i := 0 to Pred(AOutcome.Errors.Count) do begin
         if Length(AOutcome.Errors[i]) > 0 then begin
            AddIDEMessage(mluError, AOutcome.Errors[i], '', 0, 0, AView);
         end;
      end;
   end;
end;

procedure TCodeSigningHelper.DoProjectBuildingFinished(ASender: TObject; ABuildSuccessful: boolean);
var
   s: string;
   //sTargetOS: string;
   //b: boolean;
begin
   //sTargetOS := GetTargetOS;
   if GetOutputExecutableFilename(s) then begin
      if not FileExists(s) then begin
         AddIDEMessage(mluError, Format('Output file "%s" is missing!', [s]), '', 0, 0, rsCodeSigningViewCodeSign);
         Exit;
      end;
      if ABuildSuccessful then begin
         SignExecutableFileWithAllSettings(s);
         (*
         b := False;
         if TMicrosoftSignToolSigner.SupportsLazarusTargetOS(sTargetOS) then begin
            b := CodeSigningOptions.MicrosoftSignToolOptions.AutoSign;
         end else if TAppleCodeSignSigner.SupportsLazarusTargetOS(sTargetOS) then begin
            b := CodeSigningOptions.AppleCodeSignOptions.AutoSign;
         end else if TJavaKeyToolSigner.SupportsLazarusTargetOS(sTargetOS) then begin
            b := CodeSigningOptions.JavaKeyToolOptions.AutoSign;
         end;
         if b then begin
            AddIDEMessage(mluNone, rsCodeSigningStatusSigning, '', 0, 0, rsCodeSigningViewCodeSign);
            CertificateSignExecutable(s);
         end;
         if CodeSigningOptions.GnuPGOptions.AutoSign then begin
            AddIDEMessage(mluNone, rsGnuPGSigningStatusSigning, '', 0, 0, rsGnuPGSigningViewSign);
            GnuPGSignFile(s);
         end;
         CertificateSignExecutable(s);
         GnuPGSignFile(s);
         *)
      end else begin
         AddIDEMessage(mluError, rsCodeSigningStatusBuildFailed);
      end;
   end else begin
      AddIDEMessage(mluError, Format(rsCodeSigningErrorIDEMacrosSubstituteMacrosFailed, [s]));
   end;
end;

procedure TCodeSigningHelper.DoLazarusBuildingFinished(ASender: TObject; ABuildSuccessful: boolean);
begin
   if ABuildSuccessful then begin
      CertificateSignExecutable(ParamStr(0), Format(rsCodeSigningMessageIDE, [FormatDateTime('yyyy-mm-dd, hh:nn', Now)]));
   end;
end;

function TCodeSigningHelper.DoProjectOpened(ASender: TObject; AProject: TLazProject): TModalResult;
begin
   Result := mrOk;
   //CodeSigningLogInformation(AProject, 'DoProjectOpened: ' + AProject.ProjectInfoFile);
   try
      TCodeSigningProjectOptions(TCodeSigningProjectOptions.GetInstance).Read;
   except
      on E: Exception do begin
         ShowMessage('TCodeSigningHelper.DoProjectOpened'#13#10 + Format(rsCodeSigningErrorDoProjectOpened, [AProject.MainFile.Filename]));
      end;
   end;
end;

function TCodeSigningHelper.DoProjectClose(ASender: TObject; AProject: TLazProject): TModalResult;
begin
   Result := mrOk;
   try
      TCodeSigningProjectOptions(TCodeSigningProjectOptions.GetInstance).Write;
   except
      on E: Exception do begin
         ShowMessage(Format(rsCodeSigningErrorDoProjectClose, [AProject.MainFile.Filename]));
      end;
   end;
end;

function TCodeSigningHelper.CertificateSignExecutable(AFilename: string; ACustomDescription: string; AViewName: string): boolean;
begin
   Result := SignExecutableFileWithAllSettings(AFilename, ACustomDescription);
end;

function TCodeSigningHelper.CertificateVerifyExecutable(AFilename: string; AViewName: string): boolean;
begin
   Result := VerifyExecutableFileWithAllSettings(AFilename);
end;

function TCodeSigningHelper.ExtractDescriptionURL(out AURL: string): boolean;
var
   r: TRegExpr;
   sSource: string;
begin
   AURL := '';
   //    @codesigning-url(http://http://ccrdude.net/LazCodeSigningHelper/)
   sSource := LazarusIDE.ActiveProject.MainFile.GetSourceText;
   r := TRegExpr.Create('@codesigning-url\(([^\)]*)\)');
   try
      Result := r.Exec(sSource);
      if Result then begin
         AURL := r.Match[1];
      end;
   finally
      r.Free;
   end;
end;

procedure TCodeSigningHelper.DoIDEClose(Sender: TObject);
begin
   RemoveHandlers();
end;

destructor TCodeSigningHelper.Destroy;
begin
   FOtherTargetDialog.Free;
end;

procedure TCodeSigningHelper.CreateMainMenuSubMenu();
begin
   // Codesigning
   FMenuSectionCodeSigning := RegisterIDESubMenu(mnuProject, 'CodesigningMenu', rsCodeSigningMenuName);
   {$IFDEF Darwin}
   FMenuItemCodeSign := RegisterIDEMenuCommand(FMenuSectionCodeSigning, 'CodeSigningSignFile', rsCodeSigningMenuItemSignBundle, @DoCertificateSign);
   {$ELSE Darwin}
   FMenuItemCodeSign := RegisterIDEMenuCommand(FMenuSectionCodeSigning, 'CodeSigningSignFile', rsCodeSigningMenuItemSign, @DoCertificateSign);
   {$ENDIF Darwin}
   FMenuItemCodeSignOther := RegisterIDEMenuCommand(FMenuSectionCodeSigning, 'CodeSigningSignFileOther', rsCodeSigningMenuItemSignOther, @DoCertificateSignOther);
   FMenuItemCodeVerify := RegisterIDEMenuCommand(FMenuSectionCodeSigning, 'CodeSigningVerifyFile', rsCodeSigningMenuItemVerify, @DoCertificateVerify);
   FMenuItemCodeVerifyOther := RegisterIDEMenuCommand(FMenuSectionCodeSigning, 'CodeSigningVerifyFileOther', rsCodeSigningMenuItemVerifyOther, @DoCertificateVerifyOther);
   // GnuPG signing
   FMenuSectionGnuPGSigning := RegisterIDESubMenu(mnuProject, 'CodeSigningGnuPGSubMenu', rsGnuPGSigningMenuName);
end;

procedure TCodeSigningHelper.AddHandlers();
begin
   LazarusIDE.AddHandlerOnProjectBuildingFinished(@DoProjectBuildingFinished, True);
   LazarusIDE.AddHandlerOnLazarusBuildingFinished(@DoLazarusBuildingFinished, True);
   LazarusIDE.AddHandlerOnProjectOpened(@DoProjectOpened, True);
   LazarusIDE.AddHandlerOnProjectClose(@DoProjectClose, True);
   LazarusIDE.AddHandlerOnIDEClose(@DoIDEClose);
end;

procedure TCodeSigningHelper.RemoveHandlers();
begin
   LazarusIDE.RemoveHandlerOnProjectBuildingFinished(@DoProjectBuildingFinished);
   LazarusIDE.RemoveHandlerOnLazarusBuildingFinished(@DoLazarusBuildingFinished);
   LazarusIDE.RemoveHandlerOnProjectOpened(@DoProjectOpened);
   LazarusIDE.RemoveHandlerOnProjectClose(@DoProjectClose);
   LazarusIDE.RemoveHandlerOnIDEClose(@DoIDEClose);
end;

function TCodeSigningHelper.GetTargetOS: string;
var
   iBuildMode: integer;
   bm: TLazProjectBuildMode;
begin
   try
      iBuildMode := LazarusIDE.ActiveProject.LazBuildModes.IndexOf(LazarusIDE.ActiveProject.ActiveBuildModeID);
      bm := LazarusIDE.ActiveProject.LazBuildModes.BuildModes[iBuildMode];
      Result := bm.LazCompilerOptions.GetEffectiveTargetOS;
   except
      Result := '';
      {$IFDEF MSWindows}
      AddIDEMessage(mluWarning, rsCodeSigningIDEMessageTargetOSFailsFallbackWin32);
      Result := 'Win32';
      {$ENDIF MSWindows}
      {$IFDEF Darwin}
      AddIDEMessage(mluWarning, 'Unable to determine Target OS, falling back to Darwin');
      Result := 'Darwin';
      {$ENDIF Darwin}
      {$IFDEF cpujvm}
      AddIDEMessage(mluWarning, 'Unable to determine Target OS, falling back to Java');
      Result := 'Java';
      {$ENDIF cpujvm}
   end;
end;

function TCodeSigningHelper.SignExecutableFileWithSpecificSetting(AFilename: string; ASetting: TCustomCodeSigningCompilerSettings; ACustomDescription: string): boolean;
var
   sTargetOS: string;
   fs: TCustomFileSigner;
   v: TExtToolView;
   sViewName: string;
   i: integer;
begin
   sViewName := Format('Signing Output, Signer: %s, Profile: %s', [ASetting.SettingsTypeText, ASetting.ProfileName]);
   //v := MessagesView.
   //v := IDEMessagesWindow.GetView(sViewName, true);

   (*

   // this was debug output to find out about marking output groups as "green" (finished / successful)

   if Assigned(IDEMessagesWindow) then begin
      AddIDEMessage(mluVerbose, 'Assigned(IDEMessagesWindow)', '', 0, 0, 'test');
      for i := 0 to Pred(IDEMessagesWindow.ViewCount) do begin
         AddIDEMessage(mluVerbose, 'IDEMessagesWindow.View[].Caption = ' + IDEMessagesWindow.Views[i].Caption, '', 0, 0, 'test');
         AddIDEMessage(mluVerbose, 'IDEMessagesWindow.View[].ExitStatus = ' + IntToStr(IDEMessagesWindow.Views[i].ExitStatus), '', 0, 0, 'test');
         AddIDEMessage(mluVerbose, 'IDEMessagesWindow.View[].HasFinished = ' + BoolToStr(IDEMessagesWindow.Views[i].HasFinished, True), '', 0, 0, 'test');
         AddIDEMessage(mluVerbose, 'IDEMessagesWindow.View[].Running = ' + BoolToStr(IDEMessagesWindow.Views[i].Running, True), '', 0, 0, 'test');
         IDEMessagesWindow.Views[i].Running := False;
      end;
   end else begin
      AddIDEMessage(mluVerbose, 'not Assigned(IDEMessagesWindow)', '', 0, 0, 'test');
   end;

   *)
   v := nil;
   sTargetOS := GetTargetOS;
   //AddIDEMessage(mluHint, 'Processing setting: ' + ASetting.ClassName, '', 0, 0, sViewName);
   fs := nil;
   if ASetting is TMicrosoftSignToolCodeSigningCompilerSettings then begin
      if TMicrosoftSignToolSigner.SupportsLazarusTargetOS(sTargetOS) then begin
         fs := TMicrosoftSignToolSigner.Create;
         AssignOptionsToMicrosoftSignTool(AFilename, TMicrosoftSignToolSigner(fs), TMicrosoftSignToolCodeSigningCompilerSettings(ASetting));
         if Length(ACustomDescription) > 0 then begin
            TMicrosoftSignToolSigner(fs).DescriptionSubject := UTF8Decode(ACustomDescription);
         end;
         // TODO : perform tests
      end else begin
         AddIDEMessage(mluError, rsCodeSigningMSSignToolNotSupported, '', 0, 0, sViewName);
      end;
   end else if ASetting is TGnuPGCompilerSettings then begin
      if TGnuPGSigner.SupportsLazarusTargetOS(sTargetOS) then begin
         fs := TGnuPGSigner.Create;
         AssignOptionsToGnuPG(AFilename, TGnuPGSigner(fs), TGnuPGCompilerSettings(ASetting));
      end else begin
         AddIDEMessage(mluError, rsCodeSigningGnuPGNotSupported, '', 0, 0, sViewName);
      end;
   end;
   if Assigned(fs) then begin
      try
         try
            Result := fs.SignFile(UTF8Decode(AFilename));
            AddIDEMessage(mluVerbose, fs.Outcome.CommandLine, '', 0, 0, sViewName);
            AddIDEMessage(mluHint, Format(rsCodeSigningStatusErrorCode, [fs.Outcome.ExitCode]), '', 0, 0, sViewName);
            if Result then begin
               AddIDEMessage(mluImportant, Format(rsCodeSigningMessageSuccess, [AFilename]), '', 0, 0, sViewName);
            end else begin
               AddIDEMessage(mluError, Format(rsCodeSigningMessageError, [AFilename]), '', 0, 0, sViewName);
            end;
            ProcessOutputToMessage(Result, fs.Outcome, sViewName);
            if Assigned(v) then begin
               v.Running := False;
               if Result then begin
                  v.ExitStatus := 0;
                  v.PendingLines.Clear;
               end else begin
                  v.ExitStatus := -1;
               end;
            end;
         finally
            fs.Free;
         end;
      except
         on E: Exception do begin
            AddIDEMessage(mluError, E.Message, '', 0, 0, sViewName);
         end;
      end;
   end;
end;

function TCodeSigningHelper.SignExecutableFileWithAllSettings(AFilename: string; ACustomDescription: string): boolean;
var
   po: TCodeSigningProjectOptions;
   i: integer;
begin
   AddIDEMessage(mluImportant, 'SignExecutableFileWithAllSettings(''' + AFilename + ''', ''' + ACustomDescription + ''')');
   Result := True;
   po := TCodeSigningProjectOptions(TCodeSigningProjectOptions.GetInstance);
   po.Read;
   for i := 0 to Pred(po.ProjectSettings.Count) do begin
      if po.ProjectSettings[i].BuildModes.IndexOf(LazarusIDE.ActiveProject.ActiveBuildModeID) > -1 then begin
         if not SignExecutableFileWithSpecificSetting(AFilename, po.ProjectSettings[i], ACustomDescription) then begin
            Result := False;
         end;
      end;
   end;
end;

function TCodeSigningHelper.VerifyExecutableFileWithAllSettings(AFilename: string): boolean;
begin
   // TODO : implement verification
end;

procedure TCodeSigningHelper.DoCertificateSign(Sender: TObject);
var
   s: string;
begin
   if GetOutputExecutableFilename(s) then begin
      IDEMessagesWindow.Clear;
      CertificateSignExecutable(s);
   end else begin
      ShowMessage(Format(rsCodeSigningErrorIDEMacrosSubstituteMacrosFailed, [s]));
   end;
end;

procedure TCodeSigningHelper.DoCertificateSignOther(Sender: TObject);
begin
   if FOtherTargetDialog.Execute then begin
      CertificateSignExecutable(FOtherTargetDialog.FileName);
   end;
end;

procedure TCodeSigningHelper.DoCertificateVerify(Sender: TObject);
var
   s: string;
begin
   if GetOutputExecutableFilename(s) then begin
      IDEMessagesWindow.Clear;
      CertificateVerifyExecutable(s);
   end else begin
      ShowMessage(Format(rsCodeSigningErrorIDEMacrosSubstituteMacrosFailed, [s]));
   end;
end;

procedure TCodeSigningHelper.DoCertificateVerifyOther(Sender: TObject);
begin
   if FOtherTargetDialog.Execute then begin
      CertificateVerifyExecutable(FOtherTargetDialog.FileName);
   end;
end;

procedure TCodeSigningHelper.AssignOptionsToGnuPG(var AFilename: string; ASigner: TGnuPGSigner; ASetting: TGnuPGCompilerSettings);
var
   sProfile: string;
   p: TGnuPGCodeSigningProfile;
begin
   ASigner.UseASCIIArmor := ASetting.UseASCIIArmor;
   sProfile := ASetting.ProfileName;
   p := TGnuPGCodeSigningProfile(CodeSigningOptions.Profiles.FindProfile(sProfile));
   if Assigned(p) then begin
      ASigner.SigningExecutable := UTF8Decode(p.Executable);
      ASigner.UseCustomKey := True;
      ASigner.Certificate.Source := p.Source;
      ASigner.Certificate.Hash := p.SourceHash;
      ASigner.Certificate.Substring := p.SourceSubstring;
   end else begin
      AddIDEMessage(mluError, Format('Specified Profile "%s" not found!', [sProfile]), '', 0, 0, rsCodeSigningViewCodeSign);
   end;
end;

procedure TCodeSigningHelper.AssignOptionsToMicrosoftSignTool(var AFilename: string; ASigner: TMicrosoftSignToolSigner; ASetting: TMicrosoftSignToolCodeSigningCompilerSettings);
var
   sProfile: string;
   sURL: string;
   p: TMicrosoftSignToolCodeSigningProfile;
begin
   ExtractDescriptionURL(sURL);
   if ASetting.UsePageHashing then begin
      ASigner.Flags.UsePageHashing := csphYes;
   end else begin
      ASigner.Flags.UsePageHashing := csphNo;
   end;
   ASigner.Flags.UseWindowsSystemComponentVerification := ASetting.UseWindowsSystemComponenVerification;
   ASigner.DescriptionURL := ASetting.DescriptionURL;
   if Length(sURL) > 0 then begin
      ASigner.DescriptionURL := sURL;
   end;
   ASigner.Algorithms := [csaSHA256];
   if Assigned(LazarusIDE) and Assigned(LazarusIDE.ActiveProject) then begin
      ASigner.DescriptionSubject := UTF8Decode(LazarusIDE.ActiveProject.GetTitleOrName);
   end else begin
      ASigner.DescriptionSubject := '';
   end;
   sProfile := ASetting.ProfileName;
   p := TMicrosoftSignToolCodeSigningProfile(CodeSigningOptions.Profiles.FindProfile(sProfile));
   if Assigned(p) then begin
      TestMicrosoftSignToolProfile(p);
      ASigner.SigningExecutable := UTF8Decode(p.Executable);
      ASigner.Mode := p.SigningMode;
      ASigner.Certificate.Source := p.KeySHA256.Source;
      ASigner.Certificate.Hash := p.KeySHA256.Hash;
      ASigner.Certificate.Substring := p.KeySHA256.Substring;
      ASigner.Certificate.Filename := UTF8Decode(p.KeySHA256.FilenamePFX);
      ASigner.Certificate.FilePassword := UTF8Decode(p.KeySHA256.PasswordPFX);
      ASigner.ACS.EndPoint := p.KeyAzure.Endpoint;
      ASigner.ACS.AccountName := p.KeyAzure.AccountName;
      ASigner.ACS.ProfileName := p.KeyAzure.ProfileName;
      ASigner.ACS.DLibPath := p.KeyAzure.DlibPath;
      ASigner.CrossSigning.Active := p.CrossSigningActive;
      ASigner.CrossSigning.Filename := UTF8Decode(p.CrossSigningFilename);
      ASigner.Timestamping.Active := p.TimestampingActive;
      ASigner.Timestamping.Server := p.TimestampingServer;
      ASigner.Timestamping.ServerRFC3161 := p.TimestampingServer;
   end else begin
      AddIDEMessage(mluError, Format('Specified Profile "%s" not found!', [sProfile]), '', 0, 0, rsCodeSigningViewCodeSign);
   end;
end;

procedure TCodeSigningHelper.PrintMicrosoftSignToolOptions(ASigner: TMicrosoftSignToolSigner; AViewName: string);
begin
   if Length(AViewName) = 0 then begin
      AViewName := rsCodeSigningViewCodeSign;
   end;
   case ASigner.Flags.UsePageHashing of
      csphDefault:
      begin
         AddIDEMessage(mluVerbose, rsCodeSigningMessagePageHashingDefault, '', 0, 0, AViewName);
      end;
      csphNo:
      begin
         AddIDEMessage(mluVerbose, rsCodeSigningMessagePageHashingNo, '', 0, 0, AViewName);
      end;
      csphYes:
      begin
         AddIDEMessage(mluVerbose, rsCodeSigningMessagePageHashingYes, '', 0, 0, AViewName);
      end;
   end;
   AddIDEMessage(mluVerbose, Format(rsCodeSigningMessageWindowsSystemComponentVerification, [BoolToStr(ASigner.Flags.UseWindowsSystemComponentVerification, True)]), '', 0, 0, AViewName);
   AddIDEMessage(mluVerbose, Format(rsCodeSigningMessageTimestampingActive, [BoolToStr(ASigner.Timestamping.Active, True)]), '', 0, 0, AViewName);
   AddIDEMessage(mluVerbose, Format(rsCodeSigningMessageTimestampingServer, [ASigner.Timestamping.ServerRFC3161]), '', 0, 0, AViewName);
   AddIDEMessage(mluVerbose, Format(rsCodeSigningMessageCrossSigningActive, [BoolToStr(ASigner.CrossSigning.Active, True)]), '', 0, 0, AViewName);
   AddIDEMessage(mluVerbose, Format(rsCodeSigningMessageCrossSigningFile, [UTF8Encode(ASigner.CrossSigning.Filename)]), '', 0, 0, AViewName);
   AddIDEMessage(mluVerbose, Format(rsCodeSigningMessageDescriptionText, [UTF8Encode(ASigner.DescriptionSubject)]), '', 0, 0, AViewName);
   AddIDEMessage(mluVerbose, Format(rsCodeSigningMessageDescriptionURL, [ASigner.DescriptionURL]), '', 0, 0, AViewName);
end;

procedure TCodeSigningHelper.PrintCertificate(ACertificate: TCustomFileSignerCertificate; AView: string);
begin
   case ACertificate.Source of
      cscsStoreByHash:
      begin
         AddIDEMessage(mluVerbose, Format(rsCodeSigningMessageCertificateSourceHash, [ACertificate.Hash]), '', 0, 0, AView);
      end;
      cscsFileAsPFX:
      begin
         AddIDEMessage(mluVerbose, Format(rsCodeSigningMessageCertificateSourceSubstring, [ACertificate.Substring]), '', 0, 0, AView);
      end;
      cscsStoreBySubstring:
      begin
         AddIDEMessage(mluVerbose, Format(rsCodeSigningMessageCertificateSourceFile, [UTF8Encode(ACertificate.Filename)]), '', 0, 0, AView);
      end;
   end;
end;

procedure TCodeSigningHelper.TestMicrosoftSignToolProfile(AProfile: TMicrosoftSignToolCodeSigningProfile);
const SView = 'CodeSigning Parameter Verification';
begin
   if FileExists(AProfile.Executable) then begin
      AddIDEMessage(mluNote, Format('SignTool executable found: %s', [AProfile.Executable]), '', 0, 0, SView);
   end else begin
      AddIDEMessage(mluError, Format('SignTool executable missing: %s', [AProfile.Executable]), '', 0, 0, SView);
   end;
   if AProfile.CrossSigningActive then begin
      if FileExists(AProfile.CrossSigningFilename) then begin
         AddIDEMessage(mluNote, Format('SignTool cross-signing certificate found: %s', [AProfile.CrossSigningFilename]), '', 0, 0, SView);
      end else begin
         AddIDEMessage(mluError, Format('SignTool cross-signing certificate missing: %s', [AProfile.CrossSigningFilename]), '', 0, 0, SView);
      end;
   end;
   case AProfile.SigningMode of
      msstcsmLocal: begin
      end;
      msstcsmAzure: begin
         if FileExists(AProfile.KeyAzure.DLibPath) then begin
            AddIDEMessage(mluNote, Format('SignTool ACS dlib file found: %s', [AProfile.KeyAzure.DLibPath]), '', 0, 0, SView);
         end else begin
            AddIDEMessage(mluError, Format('SignTool ACS dlib file missing: %s', [AProfile.KeyAzure.DLibPath]), '', 0, 0, SView);
         end;
         if Length(AProfile.KeyAzure.EndPoint) > 0 then begin
            AddIDEMessage(mluNote, Format('SignTool ACS endpoint found: %s', [AProfile.KeyAzure.EndPoint]), '', 0, 0, SView);
         end else begin
            AddIDEMessage(mluError, 'SignTool ACS endpoint missing.', '', 0, 0, SView);
         end;
         if Length(AProfile.KeyAzure.AccountName) > 0 then begin
            AddIDEMessage(mluNote, Format('SignTool ACS account name found: %s', [AProfile.KeyAzure.AccountName]), '', 0, 0, SView);
         end else begin
            AddIDEMessage(mluError, 'SignTool ACS account name missing.', '', 0, 0, SView);
         end;
         if Length(AProfile.KeyAzure.ProfileName) > 0 then begin
            AddIDEMessage(mluNote, Format('SignTool ACS profile name found: %s', [AProfile.KeyAzure.ProfileName]), '', 0, 0, SView);
         end else begin
            AddIDEMessage(mluError, 'SignTool ACS profile name missing.', '', 0, 0, SView);
         end;
      end;
   end;
end;

class constructor TCodeSigningHelper.Create;
begin
   FInstance := nil;
end;

class destructor TCodeSigningHelper.Destroy;
begin
   FInstance.Free;
end;

class function TCodeSigningHelper.Instance: TCodeSigningHelper;
begin
   if not Assigned(FInstance) then begin
      FInstance := TCodeSigningHelper.Create;
   end;
   Result := FInstance;
end;

procedure TCodeSigningHelper.AssignOptionsToAppleCodeSign(var AFilename: string; ASigner: TAppleCodeSignSigner; AAllowProjectSpecificOptions: boolean; ACustomDescription: string; AViewName: string);
var
   sFilenameBundle: string;
   sFilenameExeInBundle: string;
   sDirnameSignature: string;
   sFilenameSignature: string;
   po: TCodeSigningProjectOptions;
begin
   if Length(AViewName) = 0 then begin
      AViewName := rsCodeSigningViewCodeSign;
   end;
   po := TCodeSigningProjectOptions(TCodeSigningProjectOptions.GetInstance);
   sFilenameBundle := AFilename + '.app';
   sDirnameSignature := sFilenameBundle + '/Contents/_CodeSignature';
   sFilenameSignature := SDirnameSignature + '/CodeResources';
   if FileExists(sFilenameSignature) then begin
      if DeleteFile(sFilenameSignature) then begin
         AddIDEMessage(mluVerbose, Format(rsCodeSigningStatusSignatureRemovedGood, [sFilenameSignature]), sFilenameSignature, 0, 0, AViewName);
      end else begin
         AddIDEMessage(mluError, Format(rsCodeSigningStatusSignatureRemovedFail, [sFilenameSignature]), sFilenameSignature, 0, 0, AViewName);
      end;
   end;
   if DirectoryExists(sDirnameSignature) then begin
      if RemoveDir(sDirnameSignature) then begin
         AddIDEMessage(mluVerbose, Format(rsCodeSigningStatusSignatureFolderRemovedGood, [sFilenameSignature]), sFilenameSignature, 0, 0, AViewName);
      end else begin
         AddIDEMessage(mluError, Format(rsCodeSigningStatusSignatureFolderRemovedFail, [sFilenameSignature]), sFilenameSignature, 0, 0, AViewName);
      end;
   end;
   if FileExists(AFilename) and DirectoryExists(sFilenameBundle) then begin
      sFilenameExeInBundle := sFilenameBundle + '/Contents/MacOS/' + ExtractFileName(AFilename);
      if FileExists(sFilenameExeInBundle) then begin
         if not DeleteFile(sFilenameExeInBundle) then begin
            AddIDEMessage(mluError, Format(rsCodeSigningStatusSymlinkRemovedFail, [sFilenameExeInBundle]), sFilenameExeInBundle, 0, 0, AViewName);
            Exit;
         end;
      end;
      if CopyFile(AFilename, sFilenameExeInBundle) then begin
         {$IFDEF Darwin}
         FpChmod(sFilenameExeInBundle, &755);
         {$ENDIF Darwin}
         AddIDEMessage(mluVerbose, Format(rsCodeSigningStatusExecutableToBundleCopiedGood, [sFilenameExeInBundle]), sFilenameExeInBundle, 0, 0, AViewName);
         if not DeleteFile(AFilename) then begin
            AddIDEMessage(mluError, Format(rsCodeSigningStatusDeleteUnbundledExecutableFail, [AFilename]), AFilename, 0, 0, AViewName);
         end;
      end else begin
         AddIDEMessage(mluError, Format(rsCodeSigningStatusExecutableToBundleCopiedFail, [sFilenameExeInBundle]), sFilenameExeInBundle, 0, 0, AViewName);
         Exit;
      end;
      AFilename := sFilenameBundle;
   end else if DirectoryExists(sFilenameBundle) then begin
      AFilename := sFilenameBundle;
   end else begin
      AFilename := AFilename;
   end;
   if po.AppleCodeSignCustom.UseSpecificCertificate and AAllowProjectSpecificOptions then begin
      AddIDEMessage(mluVerbose, rsCodeSigningMessageProjectSpecificCertificate, '', 0, 0, AViewName);
      ASigner.Certificate.Assign(po.AppleCodeSignOptions.Certificate);
   end else begin
      ASigner.Certificate.Assign(CodeSigningOptions.AppleCodeSignOptions.Certificate);
   end;
   PrintCertificate(ASigner.Certificate, AViewName);
   ASigner.SigningExecutable := CodeSigningOptions.AppleCodeSignOptions.CodeSignExecutable;
end;

constructor TCodeSigningHelper.Create;
begin
   FOtherTargetDialog := TOpenDialog.Create(nil);
   AddHandlers();
end;

initialization

   RegisterClass(TCodeSigningHelper);

   CodeSigningOptionGroup := GetFreeIDEOptionsGroupIndex(GroupEditor);
   RegisterIDEOptionsGroup(CodeSigningOptionGroup, TCodeSigningOptions);

end.
