{
   @author(Patrick Michael Kolla-ten Venne [pk] <patrick@kolla-tenvenne.de>)
   @abstract(Lazarus IDE options for codesigning.)

   @preformatted(
// *****************************************************************************
// Copyright: © 2017 Patrick Michael Kolla-ten Venne. All rights reserved.
// *****************************************************************************
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
// *****************************************************************************
// Changelog (new entries first):
// ---------------------------------------
// 2017-05-11  pk  ---  [CCD] Updated unit header.
// *****************************************************************************
   )
}

unit CodeSigningHelper.Options;

{$IFDEF FPC}
{$mode objfpc}{$H+}
{$ENDIF FPC}

interface

uses
   Classes,
   SysUtils,
   LazIDEIntf,
   IDEOptionsIntf,
   IniFiles,
   Generics.Collections,
   CodeSigningHelper.Profiles,
   PepiMK.Signing.MicrosoftSignTool,
   PepiMK.Signing.Base;

type

   { TCustomCodeSigningOptions }

   TCustomCodeSigningOptions = class(TPersistent)
   private
      FProfileName: string;
   public
      procedure LoadFromIni(AIni: TCustomIniFile; ASectionName: string); virtual;
      procedure SaveToIni(AIni: TCustomIniFile; ASectionName: string); virtual;
      property ProfileName: string read FProfileName write FProfileName;
   end;

   { TCodeSigningOptionsAppleCodeSign }

   TCodeSigningOptionsAppleCodeSign = class(TCustomCodeSigningOptions)
   private
      FAutoSign: boolean;
      FCertificate: TCustomFileSignerCertificate;
      FCodeSignExecutable: WideString;
   public
      constructor Create;
      destructor Destroy; override;
      procedure Assign(Source: TPersistent); override;
      procedure LoadFromIni(AIni: TCustomIniFile; ASectionName: string); override;
      procedure SaveToIni(AIni: TCustomIniFile; ASectionName: string); override;
      property AutoSign: boolean read FAutoSign write FAutoSign;
      property CodeSignExecutable: WideString read FCodeSignExecutable write FCodeSignExecutable;
      property Certificate: TCustomFileSignerCertificate read FCertificate;
   end;

   TCodeSigningOptionsDictionary = specialize TList<TCustomCodeSigningOptions>;

   { TCodeSigningOptions }

   TCodeSigningOptions = class(TAbstractIDEEnvironmentOptions)
   private
      FAppleCodeSignOptions: TCodeSigningOptionsAppleCodeSign;
      FOptions: TCodeSigningOptionsDictionary;
      FConfigFilename: string;
      FProfilesFilename: string;
      FProfiles: TCodeSigningProfiles;
   protected
      procedure LoadFromIni(AIni: TCustomIniFile; ASectionName: string); virtual;
      procedure SaveToIni(AIni: TCustomIniFile; ASectionName: string); virtual;
      function GetConfigFilename: string; virtual;
      function GetProfilesFilename: string; virtual;
   public
      class function GetGroupCaption: string; override;
      class function GetInstance: TAbstractIDEOptions; override;
   public
      constructor Create; virtual;
      destructor Destroy; override;
      procedure Read;
      procedure Write;
      procedure Assign(Source: TPersistent); override;
      procedure DoAfterWrite(Restore: boolean); override;
      property Profiles: TCodeSigningProfiles read FProfiles;
      property AppleCodeSignOptions: TCodeSigningOptionsAppleCodeSign read FAppleCodeSignOptions;
   end;

function CodeSigningOptions: TCodeSigningOptions;

var
   CodeSigningOptionGroup: integer;

implementation

uses
   Dialogs,
   IDEIntf,
   IDEMsgIntf,
   IDEExternToolIntf,
   CodeSigningHelper.Debug,
   CodeSigningHelper.Strings;

var
   GCodeSigningOptions: TCodeSigningOptions = nil;

const
   SToolNameAppleCodeSign = 'AppleCodeSign';

function CodeSigningOptions: TCodeSigningOptions;
begin
   if not Assigned(GCodeSigningOptions) then begin
      //CodeSigningLogInformation(GCodeSigningOptions, 'function CodeSigningOptions: TCodeSigningOptions;#Create');
      GCodeSigningOptions := TCodeSigningOptions.Create;
   end;
   Result := GCodeSigningOptions;
end;

{ TCustomCodeSigningOptions }

procedure TCustomCodeSigningOptions.LoadFromIni(AIni: TCustomIniFile; ASectionName: string);
begin
   FProfileName := AIni.ReadString(ASectionName, 'ProfileName', '');
end;

procedure TCustomCodeSigningOptions.SaveToIni(AIni: TCustomIniFile; ASectionName: string);
begin
   AIni.WriteString(ASectionName, 'ProfileName', FProfileName);
end;

{ TCodeSigningOptionsAppleCodeSign }

constructor TCodeSigningOptionsAppleCodeSign.Create;
begin
   FCertificate := TCustomFileSignerCertificate.Create;
end;

destructor TCodeSigningOptionsAppleCodeSign.Destroy;
begin
   FCertificate.Free;
   inherited Destroy;
end;

procedure TCodeSigningOptionsAppleCodeSign.Assign(Source: TPersistent);
begin
   if Source is TCodeSigningOptionsAppleCodeSign then begin
      Self.FAutoSign := TCodeSigningOptionsAppleCodeSign(Source).FAutoSign;
      Self.FCodeSignExecutable := TCodeSigningOptionsAppleCodeSign(Source).FCodeSignExecutable;
      Self.FCertificate.Assign(TCodeSigningOptionsAppleCodeSign(Source).FCertificate);
   end else begin
      inherited Assign(Source);
   end;
end;

procedure TCodeSigningOptionsAppleCodeSign.LoadFromIni(AIni: TCustomIniFile; ASectionName: string);
begin
   inherited;
   FAutoSign := AIni.ReadBool(ASectionName, 'AutoSign', True);
   FCodeSignExecutable := UTF8Decode(AIni.ReadString(ASectionName, 'Executable', 'codesign'));
   FCertificate.LoadFromIni(AIni, ASectionName);
end;

procedure TCodeSigningOptionsAppleCodeSign.SaveToIni(AIni: TCustomIniFile; ASectionName: string);
begin
   inherited;
   AIni.WriteString(ASectionName, 'Tool', SToolNameAppleCodeSign);
   AIni.WriteBool(ASectionName, 'AutoSign', FAutoSign);
   AIni.WriteString(ASectionName, 'Executable', UTF8Encode(FCodeSignExecutable));
   FCertificate.SaveToIni(AIni, ASectionName);
end;

{ TCodeSigningOptions }

function TCodeSigningOptions.GetConfigFilename: string;
begin
   if (Length(FConfigFilename) = 0) then begin
      try
         if Assigned(LazarusIDE) then begin
            FConfigFilename := IncludeTrailingPathDelimiter(LazarusIDE.GetPrimaryConfigPath) + 'CodeSigningHelper.ini';
         end;
      except
         on E: Exception do begin
            AddIDEMessage(mluError, rsCodeSigningErrorGetPrimaryConfigPath + LineEnding + E.Message, '', 0, 0, 'CodeSigning Debug');
         end;
      end;
   end;
   Result := FConfigFilename;
   //CodeSigningLogInformation(Self, 'GetConfigFilename:' + Result);
end;

function TCodeSigningOptions.GetProfilesFilename: string;
begin
   if (Length(FProfilesFilename) = 0) then begin
      try
         if Assigned(LazarusIDE) then begin
            FProfilesFilename := IncludeTrailingPathDelimiter(LazarusIDE.GetPrimaryConfigPath) + 'CodeSigningProfiles.ini';
         end;
      except
         on E: Exception do begin
            AddIDEMessage(mluError, rsCodeSigningErrorGetPrimaryConfigPath + LineEnding + E.Message, '', 0, 0, 'CodeSigning Debug');
         end;
      end;
   end;
   Result := FProfilesFilename;
   //CodeSigningLogInformation(Self, 'GetConfigFilename:' + Result);
end;

procedure TCodeSigningOptions.LoadFromIni(AIni: TCustomIniFile; ASectionName: string);
var
   sl: TStringList;
   s: string;
   sTool: string;
   t: TCustomCodeSigningOptions;
begin
   FAppleCodeSignOptions.LoadFromIni(AIni, ASectionName + '.AppleCodeSign');
   // now new options
   FOptions.Clear;
   sl := TStringList.Create;
   try
      AIni.ReadSections(sl);
      for s in sl do begin
         if LeftStr(s, Length(ASectionName)) = ASectionName then begin
            sTool := AIni.ReadString(s, 'Tool', '');
            case sTool of
               SToolNameAppleCodeSign:
               begin
                  t := TCodeSigningOptionsAppleCodeSign.Create;
                  t.LoadFromIni(AIni, ASectionName);
                  FOptions.Add(t);
               end;
            end;
         end;
      end;
   finally
      sl.Free;
   end;
end;

procedure TCodeSigningOptions.SaveToIni(AIni: TCustomIniFile; ASectionName: string);
var
   i: integer;
   sProfile: string;
   sl: TStringList;
begin
   FAppleCodeSignOptions.SaveToIni(AIni, ASectionName + '.AppleCodeSign');

   // now new
   CodeSigningLogInformation(Self, Format('TCodeSigningOptions.SaveToIni:'#13#10'Saving options to section "%s" to file %s', [ASectionName, AIni.FileName]));
   sl := TStringList.Create;
   try
      AIni.ReadSections(sl);
      for sProfile in sl do begin
         if LeftStr(sProfile, Length(ASectionName)) = ASectionName then begin
            AIni.EraseSection(sProfile);
         end;
      end;
   finally
      sl.Free;
   end;
   for i := 0 to Pred(FOptions.Count) do begin
      sProfile := FOptions.Items[i].ProfileName;
      FOptions.Items[i].SaveToIni(AIni, ASectionName + '.' + IntToStr(i));
   end;
end;

constructor TCodeSigningOptions.Create;
begin
   inherited Create;
   FConfigFilename := '';
   FProfilesFilename := '';
   FOptions := TCodeSigningOptionsDictionary.Create;
   FAppleCodeSignOptions := TCodeSigningOptionsAppleCodeSign.Create;
   FProfiles := TCodeSigningProfiles.Create;
   Read;
end;

destructor TCodeSigningOptions.Destroy;
begin
   Write;
   FOptions.Free;
   FAppleCodeSignOptions.Free;
   FProfiles.Free;
   inherited Destroy;
end;

procedure TCodeSigningOptions.Read;

var
   sFilename: string;
begin
   sFilename := GetProfilesFilename;
   if Length(sFilename) = 0 then begin
      CodeSigningLogInformation(Self, 'Unable to read options, no profiles filename available.');
      Exit;
   end;
   try
      Profiles.LoadFromIniFile(sFilename);
   except
      on E: Exception do begin
         AddIDEMessage(mluError, 'TCodeSigningOptions.Read.LoadProfiles: ' + E.Message, '', 0, 0, 'CodeSigning Debug');
      end;
   end;
end;

procedure TCodeSigningOptions.Write;
begin
   // no longer needed, frame saves itself
end;

procedure TCodeSigningOptions.Assign(Source: TPersistent);
begin
   CodeSigningLogInformation(Self, 'TCodeSigningOptions.Assign');
   if Source is TCodeSigningOptions then begin
      AppleCodeSignOptions.Assign(TCodeSigningOptions(Source).AppleCodeSignOptions);
   end else begin
      inherited Assign(Source);
   end;
end;

procedure TCodeSigningOptions.DoAfterWrite(Restore: boolean);
begin
   CodeSigningLogInformation(Self, 'TCodeSigningOptions.DoAfterWrite');
   inherited DoAfterWrite(Restore);
   Write;
end;

class function TCodeSigningOptions.GetGroupCaption: string;
begin
   Result := rsCodeSigningOptionsGroupName;
end;

class function TCodeSigningOptions.GetInstance: TAbstractIDEOptions;
begin
   Result := CodeSigningOptions;
end;

initialization

   //RegisterIDEOptionsGroup(... , TCodeSigningptions);

finalization

   GCodeSigningOptions.Free;

end.
