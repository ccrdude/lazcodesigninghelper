unit CodeSigningToolSettings.Form.GnuPG;

{$mode objfpc}{$H+}

interface

uses
   Classes,
   SysUtils,
   Forms,
   Controls,
   Graphics,
   Dialogs,
   StdCtrls,
   ButtonPanel,
   EditBtn,
   PepiMK.Signing.Base,
   CodeSigningHelper.Profiles;

type

   { TFormGnuPGProfile }

   TFormGnuPGProfile = class(TForm)
      ButtonPanel1: TButtonPanel;
      editCertificateHash: TEditButton;
      editCertificateSubjectSubstring: TEdit;
      editExecutable: TComboBox;
      editProfileName: TEdit;
      groupExecutable: TGroupBox;
      groupKey: TGroupBox;
      groupProfile: TGroupBox;
      rbSignByCertificateHash: TRadioButton;
      rbSignByCertificateSubstring: TRadioButton;
      procedure editCertificateHashButtonClick({%H-}Sender: TObject);
      procedure editCertificateHashChange({%H-}Sender: TObject);
      procedure editCertificateSubjectSubstringChange({%H-}Sender: TObject);
      procedure editExecutableChange({%H-}Sender: TObject);
      procedure editProfileNameChange({%H-}Sender: TObject);
      procedure rbSignByCertificateHashChange({%H-}Sender: TObject);
      procedure rbSignByCertificateSubstringChange({%H-}Sender: TObject);
   private
      procedure UpdateOKButtonAvailability;
   public
      procedure LoadFromProfile(const AProfile: TGnuPGCodeSigningProfile);
      procedure SaveToProfile(const AProfile: TGnuPGCodeSigningProfile);
      function Execute(const AProfile: TGnuPGCodeSigningProfile): boolean;
   end;

function ShowGnuPGProfile(const AProfile: TGnuPGCodeSigningProfile): boolean;

implementation

{$R *.lfm}

uses
   CodeSigningHelper.Certificates.Form;

function ShowGnuPGProfile(const AProfile: TGnuPGCodeSigningProfile): boolean;
var
   form: TFormGnuPGProfile;
begin
   form := TFormGnuPGProfile.Create(nil);
   try
      Result := form.Execute(AProfile);
   finally
      form.Free;
   end;
end;

{ TFormGnuPGProfile }

procedure TFormGnuPGProfile.editCertificateHashButtonClick(Sender: TObject);
var
   s: string;
begin
   s := editCertificateHash.Text;
   if SelectGnuPGCertificate(s) then begin
      editCertificateHash.Text := s;
      rbSignByCertificateHash.Checked := True;
      UpdateOKButtonAvailability;
   end;
end;

procedure TFormGnuPGProfile.editCertificateHashChange(Sender: TObject);
begin
   UpdateOKButtonAvailability;
end;

procedure TFormGnuPGProfile.editCertificateSubjectSubstringChange(Sender: TObject);
begin
   UpdateOKButtonAvailability;
end;

procedure TFormGnuPGProfile.editExecutableChange(Sender: TObject);
begin
   UpdateOKButtonAvailability;
end;

procedure TFormGnuPGProfile.editProfileNameChange(Sender: TObject);
begin
   UpdateOKButtonAvailability;
end;

procedure TFormGnuPGProfile.rbSignByCertificateHashChange(Sender: TObject);
begin
   UpdateOKButtonAvailability;
end;

procedure TFormGnuPGProfile.rbSignByCertificateSubstringChange(Sender: TObject);
begin
   UpdateOKButtonAvailability;
end;

procedure TFormGnuPGProfile.UpdateOKButtonAvailability;
var
   bValidSigningKey: boolean;
begin
   bValidSigningKey := (rbSignByCertificateHash.Checked and (Length(editCertificateHash.Text) > 0)) // ...
      or (rbSignByCertificateSubstring.Checked and (Length(editCertificateSubjectSubstring.Text) > 0));
   ButtonPanel1.OKButton.Enabled := (Length(editProfileName.Text) > 0) and (Length(editExecutable.Text) > 0) and bValidSigningKey;
end;

procedure TFormGnuPGProfile.LoadFromProfile(const AProfile: TGnuPGCodeSigningProfile);
begin
   editProfileName.Text := AProfile.ProfileName;
   editExecutable.Text := AProfile.Executable;
   editCertificateHash.Text := AProfile.SourceHash;
   editCertificateSubjectSubstring.Text := AProfile.SourceSubstring;
   rbSignByCertificateHash.Checked := (AProfile.Source = cscsStoreByHash);
   rbSignByCertificateSubstring.Checked := (AProfile.Source = cscsStoreBySubstring);
end;

procedure TFormGnuPGProfile.SaveToProfile(const AProfile: TGnuPGCodeSigningProfile);
begin
   AProfile.ProfileName := editProfileName.Text;
   AProfile.Executable := editExecutable.Text;
   AProfile.SourceHash := editCertificateHash.Text;
   AProfile.SourceSubstring := editCertificateSubjectSubstring.Text;
   if rbSignByCertificateHash.Checked then begin
      AProfile.Source := cscsStoreByHash;
   end else if rbSignByCertificateSubstring.Checked then begin
      AProfile.Source := cscsStoreBySubstring;
   end;
end;

function TFormGnuPGProfile.Execute(const AProfile: TGnuPGCodeSigningProfile): boolean;
begin
   LoadFromProfile(AProfile);
   UpdateOKButtonAvailability;
   Result := (ShowModal = mrOk);
   if Result then begin
      SaveToProfile(AProfile);
   end;
end;

end.
