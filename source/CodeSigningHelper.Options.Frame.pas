unit CodeSigningHelper.Options.Frame;

{$mode objfpc}{$H+}

interface

uses
   Classes,
   SysUtils,
   Forms,
   Controls,
   StdCtrls,
   EditBtn,
   ComCtrls,
   ExtCtrls,
   ActnList,
   Menus,
   IDEOptionsIntf;

type

   { TFrameCodeSigningOptions }

   TFrameCodeSigningOptions = class(TAbstractIDEOptionsEditor)
      aAddMicrosoftSigToolProfile: TAction;
      aAddAppleCodeSignProfile: TAction;
      aAddJavaKeyToolProfile: TAction;
      aAddGnuPGProfile: TAction;
      aDeleteProfile: TAction;
      aEditProfile: TAction;
      alCodeSigningProfiles: TActionList;
      bnAddGnuPGProfile: TButton;
      bnDeleteProfile: TButton;
      bnAddJavaKeyToolProfile: TButton;
      bnAddMicrosoftSigToolProfile: TButton;
      bnAddAppleCodeSignProfile: TButton;
      bnEditProfile: TButton;
      FlowPanel1: TFlowPanel;
      groupList: TGroupBox;
      lvCodeSigningProfiles: TListView;
      MenuItem1: TMenuItem;
      MenuItem2: TMenuItem;
      pmiDeleteProfile: TMenuItem;
      pmiEditProfile: TMenuItem;
      pmiAddGnuPGProfile: TMenuItem;
      pmiAddJavaKeyToolProfile: TMenuItem;
      pmiAddAppleCodeSignProfile: TMenuItem;
      pmiAddMicrosoftSigToolProfile: TMenuItem;
      popupCodeSigningProfiles: TPopupMenu;
   private

   public

   end;

implementation

{$R *.lfm}

uses
   CodeSigningHelper.Options,
   CodeSigningHelper.Strings,
   CodeSigningHelper.Debug,
   CodeSigningHelper.Certificates.Form;

end.
